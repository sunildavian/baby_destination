import Sequelize from 'sequelize';

const sequelize = new Sequelize('d7r7te1el6e16j', 'bfvdahtaflknzd', '4e60ca0f13cfca200067a7ec5a82e981f664a2ea35b7e73632d9a4079e4e3d32', {
    dialect: 'postgres',
    host: 'ec2-54-83-27-165.compute-1.amazonaws.com',
    port: 5432,
    "ssl":true,
    "dialectOptions":{
        "ssl":{
            "require":true
        }
    }
   /* pool: {
        max: 10,
        min: 0,
        acquire: 30000,
        idle: 10000
    }*/
});

const models = {
    User: sequelize.import('./user')
};


models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;