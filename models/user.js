const user = (sequelize, DataTypes) => {
    const User = sequelize.define('user', {
        fullname: {
            type: DataTypes.STRING
        },
        email: {
            type: DataTypes.STRING,
            unique: true
        },
        password: {
            type: DataTypes.STRING
        }
    });

    return User;
};

export default user;