import express from "express";
import models from "../models";

const jwt = require('jsonwebtoken');

var bcrypt = require('bcryptjs');

let router = express.Router();

const config = {
    secret: "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9"
}

router.post("/register", async (req, res) => {
    let {body = {}} = req;

    let {fullname, email, password} = body;
    if (!fullname || !email || !password) {
        let error = new Error("Please provide mandatory values [email/fullname/password]");
        error.code = 500;
        writeJSONResponse(error, req, res);
        return;
    }

    let emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!emailRegex.test(email)) {
        let error = new Error(`Email [${email}] is invalid`);
        error.code = 500;
        writeJSONResponse(error, req, res);
        return
    }

    try {
        let isUserExist = await models.User.findAll({
            where: {
                email
            },
            attributes: ['email']
        });
        if (isUserExist.length) {
            let error = new Error(`Email [${email}] is already exists`);
            error.code = 500;
            writeJSONResponse(error, req, res);
            return
        }

        var hashedPassword = bcrypt.hashSync(password, 8);

        let user = await models.User.create({fullname, email, password: hashedPassword})

        writeJSONResponse({id: user.id}, req, res);

    } catch (error) {
        writeJSONResponse(error, req, res);
    }

})


router.post("/log", async (req, res) => {
    let {body = {}} = req;
    let {email, password} = body;
    if (!email || !password) {
        let error = new Error("Please provide mandatory values [email/password]");
        error.code = 500;
        writeJSONResponse(error, req, res);
        return;
    }

    try {

        let isUserExist = await models.User.findAll({
            where: {
                email
            }
        })
        if (isUserExist.length === 0) {
            let error = new Error(`User with email [${email}] is not found`);
            error.code = 500;
            writeJSONResponse(error, req, res);
            return
        }

        let user = isUserExist[0];

        let isMatched = bcrypt.compareSync(password, user.password);
        if (!isMatched) {
            let error = new Error(`Authentication Failed.Check [username/password]`);
            error.code = 401;
            writeJSONResponse(error, req, res);
            return
        }

        const JWTToken = jwt.sign({
                email: user.email,
                id: user.id
            },
            config.secret,
            {
                expiresIn: '2h'
            });

        writeJSONResponse({token: JWTToken}, req, res);

    } catch (error) {
        writeJSONResponse(error, req, res);
    }

})

router.post("/profile", async (req, res) => {
    let {body = {}} = req;
    let {token, fullname} = body;
    if (!token) {
        let error = new Error(`Unauthorized`);
        error.code = 400;
        writeJSONResponse(error, req, res);
        return;
    }
    if (!fullname) {
        let error = new Error("Please provide mandatory values [fullname]");
        error.code = 500;
        writeJSONResponse(error, req, res);
        return;
    }

    try {

        let {email, id} = await verifyToken(token);

        let user = await models.User.findAll({
            where: {
                email,
                id
            }
        });

        let updateValue = await user[0].update({
            fullname
        })

        writeJSONResponse(updateValue, req, res);

    } catch (error) {
        writeJSONResponse(error, req, res);
    }

})

const verifyToken = (token) => {
    return new Promise((res, rej) => {
        jwt.verify(token, config.secret, function (err, decoded) {
            if (err) {
                let error = new Error("Invalid Token");
                error.code = 401;
                rej(error);
            }
            res(decoded);
        })
    })
}


var writeJSONResponse = (result, req, resp, options = {}) => {
    var jsonResponseType = {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "GET, POST, OPTIONS"
    };
    if (result instanceof Error) {
        var responseToWrite = {
            status: "error"
        };
        var response = {
            message: result.message,
            code: result.code,
        };
        responseToWrite.code = result.code;
        responseToWrite.response = {
            error: response
        };
        resp.writeHead(responseToWrite.code, jsonResponseType);
        resp.write(JSON.stringify(responseToWrite));
        resp.end();
    } else {
        if (result === undefined) {
            result = null;
        }
        result = JSON.stringify({response: result, status: "ok", code: 200});
        resp.writeHead(200, jsonResponseType);
        resp.write(result);
        resp.end();
    }
};


module.exports = router;