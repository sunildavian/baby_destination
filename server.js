import express from "express";
import http from "http";
import userRouter from "./routes/user";

const app = express();

import models from './models';


app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use('/users', userRouter);

let server = http.createServer(app);

const PORT = process.env.PORT || 8080;

models.sequelize.sync().then(() => {
    server.listen(PORT, () => {
        console.log(`server is started n port ${PORT}`)
    })
}).catch(err => {
    console.log(`err>>>>>>>>>>.`, err);
})
